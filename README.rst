========================
noticias-project
========================

Es un proyecto para Django 1.6 (con tag de Django 1.5).

Para usar este proyecto use los siguientes pasos:

#. Crea tu ambiente de desarrollo
#. Clona el repositorio noticias
#. Instala los requerimientos basicos
#. Configura el proyecto para la documentación con Sphinx
#. Genera la documentación del proyecto
#. Notas del Proyecto


Crea tu ambiente de desarrollo
==============================

Virtualenv
----------

Asegurate de tener virtualenv (http://www.virtualenv.org). Si ya
lo tienes instalado, crea tu virtualenv::

    $ virtualenv --distribute venv_noticias

Activa tu ambiente
    $ . venv_noticias/bin/activate

Clona el repositorio noticias
=============================

Clona el repositorio con el siguiente comando::

    $ git clone https://hernanramirez@bitbucket.org/hernanramirez/noticias.git

*nota: Detalla que el repositorio tiene una estructura especifíca, es a donde queremos
llevar el repo ringTu. 

Entra en el directorio noticias/noticias_project
    
    $ cd noticias/noticias_project

Exporta el setting de Django

    $ export DJANGO_SETTINGS_MODULE=noticias_project.settings.local

Luego retorna el directorio noticias

    $ cd ..

Instala los requerimientos básicos
==================================

Depending on where you are installing dependencies:

Para desarrollo::

    $ pip install -r requirements/local.txt

Para producción::

    $ pip install -r requirements.txt

*nota: Instala solo desarrollo. Producción solo lo dejo como referencia

Configura el proyecto para la documentación con Sphinx
======================================================

Entra en el directorio del repo noticias

    $ cd noticias

Luego crea la configuración sphinx

    $ sphinx-apidoc -A "J. Hernan Ramirez R" -F -o Docs noticias_project/

Doc:: es el directorio donde se generará la documentación
noticias_proyect: es el directio donde está el código de nuestra aplicación

Luego entra al directorio Docs

    $ cd Doc

y edita el archivo conf.py

    $ vi conf.py

en la línea #23 coloca:

.. code:: python

    sys.path.insert(0, os.path.abspath('/home/hernanr/myCode/python/RingTu/noticias/noticias_project'))

*nota: esta línea es para integrar el path de nuestro proyecto a Sphinx

Un toque de elegancia
---------------------

Sphinx maneja un template básico, existe un template bien chevere y adapatativo:  _`sphinx_rtd_theme`: https://github.com/snide/sphinx_rtd_theme https://github.com/snide/sphinx_rtd_theme

Para activarlo en el archivo ``conf.py``:

Comenta la línea #106 y coloca:

.. code:: python

    import sphinx_rtd_theme

    html_theme = "sphinx_rtd_theme"

    html_theme_path = [sphinx_rtd_theme.get_html_theme_path()]


Genera la documentación del proyecto
====================================

Por último generaremos la documentación del proyecto. Ubícate en el directorio Dosc y ejecuta el siguiente comando

    $ make html

Esto generará la documentación en el directorio _build/html

Para limpiar la documentación:

    $ make clean

Y para generarla de nuevo

    $ make html

*nota: Es muy importante destacar que el código debe  estar documentado en noticas_app/models.py y en noticas_app/views.py se tienen algunos ejemplos de coumentación de código

Notas del Proyecto
==================

Estimados, la idea de esta documentación que es nuestro projecto tenga esta misma característica, incluyendo el README.rst

Para profundizar un poco mas de Sphinx y sus etiquetas lee: https://pythonhosted.org/an_example_pypi_project/sphinx.html